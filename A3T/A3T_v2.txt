<!-- AimMatic Voice Insights - Web Receiver - Default Dark Mode - Square UI Loads From Body Tag a3t-v2 -->
<!-- Copy and paste this code into the <HEAD> -->
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.0/jquery.min.js'></script>
<script>
    var element_param = {
        'apikey'                    : 'oiEiarJl3nc0sWKP1gEm1QDe', //String, create your API key at https://account.aimmatic.com/ and create your login at http://bit.ly/2CWWl1f
        'button_background_color'   : '#191919', //Hexadecimal value https://www.w3schools.com/colors/colors_hex.asp
        'background_color'          : '#191919', //Hexadecimal value https://www.w3schools.com/colors/colors_hex.asp
        'button_text_color'         : '#FF9500', //Hexadecimal value https://www.w3schools.com/colors/colors_hex.asp
        'asset_color'               : '#FF9500', //Hexadecimal value https://www.w3schools.com/colors/colors_hex.asp
        'text_color'                : '#FFFFFF', //Hexadecimal value https://www.w3schools.com/colors/colors_hex.asp
        'thank_you_text_color'      : '#FFFFFF', //Hexadecimal value https://www.w3schools.com/colors/colors_hex.asp
        'speech_language'           : 'default', //String, value must be default for user-selected language or see Read Me for list of supported values
        'button_icon'               : 'mic', //String, value must be mic or record_voice
        'button_text'               : 'default', //String, value must be default or max 14 char
        'dialog_status'             : 'active', //String, value must be active or inactive
        'dialog_1_text'             : 'default', //String, value must be default or max 240 char
        'dialog_2_text'             : 'default', //String, value must be default or max 240 char
        'thank_you_text'            : 'default', //String, value must be default or max 18 char
    }
</script>

<!-- AimMatic Voice Insights - Web Receiver - Default Light Mode - Square UI Loads From Body Tag a3t-v2 -->
<!-- Copy and paste this code into the <HEAD> -->
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.0/jquery.min.js'></script>
<script>
    var element_param = {
        'apikey'                    : 'oiEiarJl3nc0sWKP1gEm1QDe', //String, create your API key at https://account.aimmatic.com/ and create your login at http://bit.ly/2CWWl1f
        'button_background_color'   : '#FAFAFA', //Hexadecimal value https://www.w3schools.com/colors/colors_hex.asp
        'background_color'          : '#FAFAFA', //Hexadecimal value https://www.w3schools.com/colors/colors_hex.asp
        'button_text_color'         : '#007AFF', //Hexadecimal value https://www.w3schools.com/colors/colors_hex.asp
        'asset_color'               : '#007AFF', //Hexadecimal value https://www.w3schools.com/colors/colors_hex.asp
        'text_color'                : '#000000', //Hexadecimal value https://www.w3schools.com/colors/colors_hex.asp
        'thank_you_text_color'      : '#9EA9B4', //Hexadecimal value https://www.w3schools.com/colors/colors_hex.asp
        'speech_language'           : 'default', //String, value must be default for user-selected language or see Read Me for list of supported values
        'button_icon'               : 'mic', //String, value must be mic or record_voice
        'button_text'               : 'default', //String, value must be default or max 14
        'dialog_status'             : 'active', //String, value must be active or inactive
        'dialog_1_text'             : 'default', //String, value must be default or max 240 char
        'dialog_2_text'             : 'default', //String, value must be default or max 240 char
        'thank_you_text'            : 'default', //String, value must be default or max 18 char
    }
</script>


<!-- Copy and paste this code into the <BODY> before the end -->
<script type='text/javascript' src="https://widget.aimmatic.com/a3t-v2/js"></script>