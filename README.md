# Quick Start

This README helps you get started with this repository.

---

## Signup
[Community plans are 100% Free](https://www.aimmatic.com/store/c3/Select_a_Community/), please note that some features will require a Paid Plan

---

## Create an API key
[Login to the User Portal](https://account.aimmatic.com/) and navigate to My Apps. Instructions how to create an API key are available in the API reference found on the User Portal

---

## Install the Javascript Snippet
Install the javascript snippet to add a Natural Voice Reciever to your digital property, we also offer an extension for [Adobe Experience Cloud](https://www.adobeexchange.com/experiencecloud.details.100583.html)

---

## Receive Daily Insights and Live Insights
Daily Insights are delivered by email every 24 hours and Live Insights are available in our [iOS app](https://itunes.apple.com/us/app/live-insights/id1447878627#?platform=iphone)

---

## Build More Powerful Applications
[Natural Voice SDK Java](https://github.com/aimmatic/natural-voice-sdk-java/) helps developers to quickly build powerful Natural Voice applications for your organization

---

## Supported Languages
We recommend to leave the default parameter for 'speech_language' parameter so the reciever will display the UI for language select, alternatively developers can specify one of the following [BCP 47 tags](https://tools.ietf.org/html/bcp47) to remove the UI for language select, please only one BCP 47 tag, e.g., for English (United Kingdom) use en-GB

BCP 47 tags --> Description

de-DE	German (Germany)

en-US	English (United States)

en-PH	English (Philippines)

en-AU	English (Australia)

en-CA	English (Canada)

en-GH	English (Ghana)

en-GB	English (United Kingdom)

en-IN	English (India)

en-IE	English (Ireland)

en-KE	English (Kenya)

en-NZ	English (New Zealand)

en-NG	English (Nigeria)

en-ZA	English (South Africa)

en-TZ	English (Tanzania)

es-AR	Spanish (Argentina)

es-BO	Spanish (Bolivia)

es-CL	Spanish (Chile)

es-CO	Spanish (Colombia)

es-CR	Spanish (Costa Rica)

es-EC	Spanish (Ecuador)

es-SV	Spanish (El Salvador)

es-ES	Spanish (Spain)

es-US	Spanish (United States)

es-GT	Spanish (Guatemala)

es-HN	Spanish (Honduras)

es-MX	Spanish (Mexico)

es-NI	Spanish (Nicaragua)

es-PA	Spanish (Panama)

es-PY	Spanish (Paraguay)

es-PE	Spanish (Peru)

es-PR	Spanish (Puerto Rico)

es-DO	Spanish (Dominican Republic)

es-UY	Spanish (Uruguay)

es-VE	Spanish (Venezuela)

fr-FR	French (France)

fr-CA	French (Canada)

it-IT	Italian (Italy)

ja-JP	Japanese (Japan)

ko-KR	Korean (South Korea)

pt-BR	Portuguese (Brazil)

pt-PT	Portuguese (Portugal)

cmn-Hans-HK	Chinese, Mandarin (Simplified Hong Kong)

cmn-Hans-CN	Chinese, Mandarin (Simplified China)

yue-Hant-HK	Chinese, Cantonese (Traditional Hong Kong)

cmn-Hant-TW	Chinese, Mandarin (Traditional Taiwan)


